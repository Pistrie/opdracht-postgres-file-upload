const express = require("express");
const upload = require("express-fileupload");
const bodyParser = require("body-parser");
const imagesController = require("./controllers/images.controller");

const app = express();

app.use(upload());

app.use(bodyParser.json());

app.get("/images", imagesController.getAllImages);

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.post("/", imagesController.addImage);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Listening on port: ${PORT}`));
