Commands used:

Initialize sequelize  
`npx sequelize-cli init`

After editing the config file  
`npx sequelize-cli db:create`

Generate the image object  
`npx sequelize-cli model:generate --name Image --attributes imageName:string,imageType:string,imageData:blob`

Persist the table in the database  
`npx sequelize-cli db:migrate`
