const { Router } = require("express");
const imagesController = require("../controllers/images.controller");
const router = Router();

router.get("/images", (req, res) => imagesController.getAllImages);

module.exports = router;
