const models = require("../models");
const Image = models.Image;

const getAllImages = async (req, res) => {
  console.log("inside getAllImages");
  try {
    const images = await Image.findAll()
      .then((images) => {
        images.map((image) => {
          const imageData = image.imageData.toString("base64");
          image["imageData"] = imageData;
        });
        return images;
      })
      .then((images) => {
        return res.status(200).json({ images: images });
      });
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const addImage = async (req, res) => {
  console.log("inside addImage");
  try {
    console.log(req.files);
    const image = await Image.create({
      imageType: req.files.file.mimetype,
      imageName: req.files.file.name,
      imageData: req.files.file.data,
    });
    return res.status(201).json({ status: "success" });
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
};

module.exports = {
  getAllImages,
  addImage,
};
